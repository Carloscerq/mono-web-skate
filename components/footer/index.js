import styles from './footer.module.css';

export default function Footer() {
  return (
    <footer className={styles.footerContainer}>
      <h2>Skateshop</h2>
    </footer>
  )
}
